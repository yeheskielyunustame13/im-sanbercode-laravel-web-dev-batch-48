@extends('layout.master')
@section('judul')
    tampil cast
@endsection

@section('content')
<a href="/cast/create" class="btn btn-sm btn-primary my-3">Tambah Cast</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
        <tr>
            <th scope="row">{{$key +1}}</th>
            <td>{{$item -> nama}}</td>
            <td>{{$item -> umur}}</td>
            <td>
                <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm" >Detail data</a>
            </td>
          </tr>
        @empty
            
        @endforelse
    </tbody>
  </table>
@endsection