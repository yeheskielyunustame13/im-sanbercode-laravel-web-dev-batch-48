@extends('layout.master')
@section('judul')
    tambah cast
@endsection

@section('content')
<form method="post" action="/cast">
    @csrf
    <div class="form-group">
      <label>Nama cast</label>
      <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror" placeholder="masukkan nama">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Umur</label>
      <input type="int" name="umur" class="form-control @error('umur') is-invalid @enderror" placeholder="masukkan umur">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>bio</label>
        <textarea name="bio" id="" cols="30" rows="10" class="form-control @error('bio') is-invalid @enderror"></textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection