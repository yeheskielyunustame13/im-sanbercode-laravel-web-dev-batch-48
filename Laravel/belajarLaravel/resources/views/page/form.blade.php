@extends('layout.master')
@section('judul')
    Daftar Biodata
@endsection

@section('content')
<h1>Buat Account Baru!</h1>
<h2>Sign Up Form</h2>
<form action="/welcome" method="post">
    @csrf
    <label>First name:</label> <br>
    <input type="text" name="name"> <br> <br>
    <label>Last name:</label> <br>
    <input type="text" name="lname"> <br> <br>
    <label>Gender:</label> <br>
    <input type="radio" name="Gender:"> Male <br> 
    <input type="radio" name="Gender:"> Famale <br>
    <input type="radio" name="Gender:"> Other <br> <br>
    <label>Nasionality:</label> <br>
    <select name="Nasionality:">
        <option value="">Indonesian</option>
        <option value="">Singapuran</option>
        <option value="">Malaysian</option>
        <option value="">Australian</option>
    </select> <br> <br>
    <label>Languange Spoken:</label> <br>
    <input type="checkbox" name="Languange Spoken"> Bahasa Indonesia <br>
    <input type="checkbox" name="Languange Spoken"> English <br>
    <input type="checkbox" name="Languange Spoken"> Other <br> <br>
    <label>Bio:</label> <br>
    <textarea cols="30" rows="10"></textarea> <br> <br>

    <input type="submit" value="Sign Up">
</form>

@endsection
    
