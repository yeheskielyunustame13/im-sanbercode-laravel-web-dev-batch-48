<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.tambah');
    }
    public function store(Request $request)
    {
        // validasi 
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        // masukkan data ke database 
        DB::table('cast')->insert([
            'nama' => $request->input('nama'),
            'umur' => $request->input('umur') ,
            'bio' => $request->input('bio') 
        ]);
        // arahkan ke halaman cast
        return redirect('/cast');
    }
    public function index()
    {
        $cast = DB::table('cast')->  get();
        return view('cast.tampil', ['cast'=>$cast]) ;
    }

    public function show($id)
    {
        $castData = DB::table('cast')->find($id);
        return view('cast.detail', ['castData'=>$castData]) ;
    }
}
