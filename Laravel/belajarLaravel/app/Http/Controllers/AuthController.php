<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar()
    {
        return view('page.form');
    }
    public function welcome(request $request)
    {
       $namDepan = $request->input('name');
       $namaBelakang = $request->input('lname');

       return view('page.welcome',['namaDepan' => $namDepan, 'namaBelakang' => $namaBelakang]);
    }
}
