<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class,'utama']);
Route::get('/register', [AuthController::class,'daftar']);

route::post('/welcome', [AuthController::class,'welcome']);

route::get('/data-tables', function(){
    return view('page.data-tables');
});
route::get('/table', function(){
    return view('page.table');
});

// creatae data 
route::get('/cast/create', [CastController::class, 'create']);

//validasi dan insert ke database
route::post('/cast', [CastController::class, 'store']);

// menampilkan semua data di database 
route::get('/cast', [CastController::class, 'index']);

// detail data berdasar id
route::get('/cast/{id}', [CastController::class, 'show']);